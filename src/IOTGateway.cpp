#include "IOTGateway.h"
#include <Arduino.h>
#include <WiFi.h>

IOTGateway::IOTGateway(const AWSParameters *awsParameters):
    parameters(awsParameters),
    aws()
{
}

void IOTGateway::initialiseWifi()
{
    Serial.println("Attempting to connect to WiFi.");
    WiFi.mode(WIFI_STA);
    WiFi.begin(parameters->ssid, parameters->password);
    while (WiFi.status() != WL_CONNECTED) {
        if (WiFi.status() == WL_CONNECT_FAILED) {    
            Serial.println("Failed to connect to WIFI.");
            delay(2500);
        }
    }
    Serial.println("Connected to Wifi.");
}

void IOTGateway::initialiseAWS() { 
  Serial.println("Attempting AWS connection...");
  if(aws.connect(
        parameters->hostAddress,
        parameters->clientId,
        parameters->rootCa,
        parameters->clientCert,
        parameters->privateKey)== 0) {
    Serial.println("AWS connected.");
  } else {
    Serial.println("AWS connection failed.");
    while(1);
  }   
}

void IOTGateway::initialise() {
    initialiseWifi();
    delay(2500);
    initialiseAWS();
}

void IOTGateway::subscribe(const char* topic, pSubCallBackHandler_t callBackHandler) {
    Serial.print("Subscribing to ");
    Serial.println(topic);
    if(0 == aws.subscribe(topic, callBackHandler)) {
      Serial.println("Subscribed.");
    } else {
      Serial.println("Subscribe failed."); 
      while(1);
    }
}

void IOTGateway::publish(const char* topic, const char* payload) {
    if(aws.publish(topic, payload) == 0) {        
        Serial.print("Publish:");
        Serial.println(payload);
    } else {
        Serial.println("Publish failed.");
    }
}