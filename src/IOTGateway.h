#include "AWSParameters.h"
#include <AWS_IOT.h>

#ifndef IOTGATEWAY_H
#define IOTGATEWAY_H 1

class IOTGateway {
    private:
        const AWSParameters *parameters;
        AWS_IOT aws;
        void initialiseWifi();
        void initialiseAWS();

    public:
        IOTGateway(const AWSParameters *parameters);
        void initialise();
        void subscribe(const char* topic, pSubCallBackHandler_t callBackHandler);
        void publish(const char *topic, const char* payload);
};

#endif