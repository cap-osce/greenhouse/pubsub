#include "AWSParameters.h"

AWSParameters::AWSParameters(const char* host, const char* ssid, const char* password, const char* clientId, const char* rootCa, const char* clientCert, const char* privateKey):
        hostAddress(host), 
        ssid(ssid), 
        password(password),
        clientId(clientId), 
        rootCa(rootCa),
        clientCert(clientCert),
        privateKey(privateKey) 
{

}