#include <Arduino.h>
#include "Sensor.h"
#include <ArduinoJson.h>

Sensor::Sensor(const char* name, const char* type, const int readingFrequency, const int sendingFrequency) :
    name(name),
    type(type),
    readingFrequency(readingFrequency),
    sendingFrequency(sendingFrequency)
{

}

void Sensor::formatData(char *buffer, size_t bufferSize, float sensorValue, const char* sensorId, const char* timestamp = "") {
    char value_txt[32];
    StaticJsonDocument<1024> doc;
    
    JsonObject metadata = doc.createNestedObject("metadata");
    JsonObject sensor = doc.createNestedObject("sensor");
    JsonObject data = doc.createNestedObject("data");
    JsonObject config = doc.createNestedObject("config");
    
    metadata["name"] = name;
    metadata["type"] = "EVENT";
    metadata["timestamp"] = timestamp;
    metadata["sourceId"] = sensorId;
    metadata["payloadType"] = type;

    sensor["name"] = name;
    sensor["type"] = type;  

    sprintf(value_txt, "%f", sensorValue);
    data["data"] = value_txt;

    config["dataReadingFrequency"] = sendingFrequency;
    config["dataSendingFrequency"] = readingFrequency;

    serializeJson(doc, buffer, bufferSize);
}