#ifndef AWSPARAMETERS_H
#define AWSPARAMETERS_H 1

class AWSParameters {
    private:
        const char* hostAddress;
        const char* ssid;
        const char* password;
        const char* clientId;
        const char* rootCa;
        const char* clientCert;
        const char* privateKey;

    public:
        AWSParameters(
            const char* host,
            const char* ssid,
            const char* password,
            const char* clientId,
            const char* rootCa,
            const char* clientCert,
            const char* privateKey);

    friend class IOTGateway;
};

#endif