#ifndef SENSOR_DATA_H
#define SENSOR_DATA_H 1

#include<stddef.h>

class Sensor {
    private:
        const char* name;
        const char* type;
        const int readingFrequency;
        const int sendingFrequency;

    public:
        Sensor(const char* name, const char* type, const int readingFrequency, const int sendingFrequency);
        void formatData(char *buffer, size_t bufferSize, float sensorValue, const char* sensorId, const char* timestamp);
};

#endif