# pubsub

Library for publishing and subscribing with AWS IOT core MQTT server.

Based on the AWS_IOT library: https://platformio.org/lib/show/2071/AWS_IOT